
clear all;
clc;

%%%%%%%% accMagn %%%%%%%%%%
load 'PDmats/accMagn.mat';
matrixPD = accMagn;
load 'OHmats/accMagn.mat';
matrixOH = accMagn;
YTickLabel = 'mag_{\alpha}';
%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%% rvelMagn %%%%%%%%%%
load 'PDmats/rvelMagn.mat';
matrixPD = rvelMagn;
load 'OHmats/rvelMagn.mat';
matrixOH = rvelMagn;
YTickLabel = 'mag_{\omega}';
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%% accSumDiff %%%%%%%%%%
load 'PDmats/accSumDiff.mat';
matrixPD = accSumDiff;
load 'OHmats/accSumDiff.mat';
matrixOH = accSumDiff;
YTickLabel = 'sd_{\alpha}';
%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
%%%%%%% rvelAmp %%%%%%%%%%
load 'PDmats/rvelAmp.mat';
matrixPD = rvelAmp;
load 'OHmats/rvelAmp.mat';
matrixOH = rvelAmp;
YTickLabel = 'mAmp_{\omega}';
%%%%%%%%%%%%%%%%%%%%%%%%%%



nOH = numel(matrixOH(:,1));
nPD = numel(matrixPD(:,1));

datarR = [matrixOH(:,1); matrixPD(:,1)];
dataeR = [matrixOH(:,2); matrixPD(:,2)];
datarL = [matrixOH(:,3); matrixPD(:,3)];
dataeL = [matrixOH(:,4); matrixPD(:,4)];

group = [ones(nOH,1); 2*ones(nPD,1)];

subplot(2,2,1);
boxplot(datarR, group, 'Labels', {'SmartH2','SmartPD2'});
title('Rest Right (restR)');
ylabel(YTickLabel);

subplot(2,2,2);
boxplot(dataeR, group, 'Labels', {'SmartH2','SmartPD2'});
title('Extension Right (extR)');
ylabel(YTickLabel);

subplot(2,2,3);
boxplot(datarL, group, 'Labels', {'SmartH2','SmartPD2'})
title('Rest Left (restL)');
ylabel(YTickLabel);

subplot(2,2,4);
boxplot(dataeL, group, 'Labels', {'SmartH2','SmartPD2'})
title('Extension Left (extL)');
ylabel(YTickLabel);


